import React from "react";
import SearchIcon from '@material-ui/icons/Search';

class Search extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            filter: "search",
            filterby: ""
        }
        this.getMatchedList = this.getMatchedList.bind(this);
    }
    getMatchedList(event){
        
        this.setState({ filterby: event.target.value }, () => {
            console.log(this.state.filterby)
            this.props.searchData(this.state.filter, this.state.filterby)
        });
    }

    render(){
        return (
            <div>
                <SearchIcon />
                <input className="search-bar"
                placeholder="Search..."
                onChange={this.getMatchedList} />
            </div>
            
            
        );
    }
}

export default Search;