import React from "react";
import Author from "../Author/Author";
import Close from "../Close/Close";
import Labels from "../Labels/Labels";
import Open from "../Open/Open";
import Sort from "../Sort/Sort";
import sortJsonData from "sort-json-array"
import Search from "../Search/Search";

class FilteredIssue extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            issues: [],
            filterIssues: [],
            currentFilter: "open",
            getData: false
        }
        this.getListOfIssues = this.getListOfIssues.bind(this)
        this.getFilter = this.getFilter.bind(this)
        this.getOpenedOrClosedIssueList = this.getOpenedOrClosedIssueList.bind(this)
        this.getSortedIssueList = this.getSortedIssueList.bind(this)
        this.getIssueListByAuthor = this.getIssueListByAuthor.bind(this)
        this.getIssueListByLabels = this.getIssueListByLabels.bind(this)
        this.getIssueListBySearch = this.getIssueListBySearch.bind(this)
    }

    componentDidMount(){
        this.setState({issues: this.props.issues})
    }

    getListOfIssues(value){
        this.setState({issues: value})
        this.props.filteredList(this.state.issues)
    }

    getFilter(newFilter, filterby){
        this.setState({currentFilter: newFilter}, 
            () => {                
                if(this.state.currentFilter && filterby){
                    switch(this.state.currentFilter){
                        case 'Sort':
                            this.getSortedIssueList(filterby)
                            break;
                        case 'Author':
                            this.getIssueListByAuthor(filterby)
                            break;
                        case 'Labels':
                            this.getIssueListByLabels(filterby)
                            break;
                        case 'search':
                            this.getIssueListBySearch(filterby)
                            break;
                        default:
                            console.log("error")
                    }
                }
                else {
                    this.getOpenedOrClosedIssueList()
                }
                
            });
        
    }

    getOpenedOrClosedIssueList(){
        this.setState({filterIssues: this.state.issues.filter((item) => (item.state === this.state.currentFilter))}, 
        () => {
            console.log("Hello from if", this.state.filterIssues)
            this.props.filteredlist(this.state.filterIssues)
        })
    }

    getSortedIssueList(filterby){
        if(filterby === 'newest'){
            let data = sortJsonData(this.state.issues, "created_at", 'des')
            this.setState({filterIssues: data}, () => this.props.filteredlist(this.state.filterIssues))            
        }
        else if(filterby === 'oldest'){
            let data = sortJsonData(this.state.issues, "created_at", 'asc')
            this.setState({filterIssues: data}, () => this.props.filteredlist(this.state.filterIssues))
        }
        else if(filterby === 'recently updated'){
            let data = sortJsonData(this.state.issues, "updated_at", 'des')
            this.setState({filterIssues: data}, () => this.props.filteredlist(this.state.filterIssues))
        }
        else if(filterby === 'least recently updated'){
            let data = sortJsonData(this.state.issues, "updated_at", 'asc')
            this.setState({filterIssues: data}, () => this.props.filteredlist(this.state.filterIssues))
        }
    }

    getIssueListByAuthor(filterby){
        this.setState({filterIssues: this.state.issues.filter((item) => (item.user.login === filterby))}, 
        () => {
            console.log("Hello from if", this.state.filterIssues)
            this.props.filteredlist(this.state.filterIssues)
        })
    }

    getIssueListByLabels(filterby){
        var lists = []
        this.state.issues.map((item) => {
            if(item.labels.length > 0){
                for(let i = 0; i < item.labels.length; i++){
                    if(item.labels[i].name === filterby){
                        lists.push(item)
                    }
                }                
            }
        })
        this.setState({filterIssues: lists}, () => this.props.filteredlist(this.state.filterIssues))
    }

    getIssueListBySearch(filterby){
        console.log("filter", filterby)
        const regex = new RegExp(filterby, 'i');
        console.log("regex", regex)
        this.setState({filterIssues: this.state.issues.filter(item => item.title.match(regex))},
        () => {
            this.props.filteredlist(this.state.filterIssues)
        })
    }

    render(){       
        return (
            <div className="issue-header">
                <Open openedData = {this.getFilter} />
                <Close closedData = {this.getFilter}/>
                <Search searchData = {this.getFilter}/>
                <Author authorData = {this.getFilter} issue = {this.props.issues}/>
                <Labels labelData = {this.getFilter} issue = {this.props.issues}/>
                <Sort sortData = {this.getFilter} />
            </div>
        );
    }
}

export default FilteredIssue;