import React from "react";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

class Labels extends React.Component{
    constructor(props) {
        super(props);
        this.state = { filter: 'Labels', filterby: "" };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        this.setState({ filterby: event.target.value }, ()=> {
            this.props.labelData(this.state.filter, this.state.filterby)});
    }

    render(){
        const data = this.props.issue
        return (
            <div class="dropdown-list">
                <Select value={this.state.filter} onChange={this.handleChange} name="Label">
                    <MenuItem value="Labels" disabled>Label</MenuItem>
                    {Array.isArray(data) && data.map((object, index) => (
                        (object.labels).map((label, index) => (
                            <MenuItem key={index} value={label.name}>
                            {label.name}
                            </MenuItem>
                        ))
                    ))
                    }   
                </Select>
            </div>
        );
    }

}

export default Labels;