import React from "react";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

class Author extends React.Component{
    constructor(props) {
        super(props);
        this.state = { filter: 'Author', filterby: "" };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        console.log(event.target.value)
        this.setState({ filterby: event.target.value }, ()=> {
            this.props.authorData(this.state.filter, this.state.filterby)
        });
    }

    render(){
        const data = this.props.issue
        // console.log("Author" , data)
        return (
            <div class="dropdown-list">
                <Select value={this.state.filter} onChange={this.handleChange} name="Author">
                    <MenuItem value="Author" disabled>Author</MenuItem>
                    {Array.isArray(data) && data.map((object, index) => (
                        <MenuItem key={index} value={object.user.login}>
                            {object.user.login}
                        </MenuItem>
                        ))
                        }  
                </Select>
            </div>
        );
    }
}

export default Author;