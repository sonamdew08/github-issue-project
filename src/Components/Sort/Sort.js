import React from "react";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

class Sort extends React.Component{
    constructor(props) {
        super(props);

        this.state = { filter: 'Sort', filterby:"" };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        this.setState({ filterby: event.target.value }, ()=>{
            this.props.sortData(this.state.filter, this.state.filterby)
        });
    }

    render(){
        
        const sortIssue = ['newest', 'oldest', 'recently updated', 'least recently updated']
        return (
            <div class="dropdown-list">
                <Select value={this.state.filter} onChange={this.handleChange} name="Sort">
                    <MenuItem value="Sort" disabled>Sort</MenuItem>
                    {sortIssue.map((elems, index) => (
                        <MenuItem key={index} value={elems}>{elems}</MenuItem>
                    ))}    
                </Select>
            </div>
            // <div>
            //     <select value={this.state.filter} onChange={this.handleChange}>
            //     <option value="Sort" disabled>Sort</option>
            //         {sortIssue.map((elems, index) => (
            //             <option key={index} value={elems}>{elems}</option>
            //         ))}                    
            //     </select>
            // </div>
        );
    }
}
export default Sort;