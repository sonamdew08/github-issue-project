import React from 'react';
import './App.css';
import IssueData from "./Components/IssueData/IssueData"

class App extends React.Component {
           
    render(){
        return (
            <IssueData />
        );
    }
}

export default App;